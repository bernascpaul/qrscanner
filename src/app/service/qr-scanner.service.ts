import { Injectable } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

class qrCode {
  message: string;
  date: Date;
}

@Injectable({
  providedIn: 'root'
})
export class QrScannerService {
  qrCodeScanned: Array<qrCode> = [];

  constructor(public barcodeScanner: BarcodeScanner) {  }

  qrScan() {
    this.barcodeScanner.scan().then(barcodeData => {
      if (barcodeData.text != ''){
        this.qrCodeScanned.unshift({
          message: barcodeData.text,
          date: new Date(),
        });
        return;
      }
    })
    .catch(err => {
      console.log("Error", err);
    });
  }
}
