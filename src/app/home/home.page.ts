import { Component } from '@angular/core';
import { QrScannerService } from './../service/qr-scanner.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public qrScanner: QrScannerService) {  }

  qrScan() {
    this.qrScanner.qrScan();
  }
}
